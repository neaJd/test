const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
var Mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
var fs = require('fs');

MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
  if (err) throw err;
  console.log("Database created!");
    var dbo = db.db("mydb");
  dbo.createCollection("customers", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
  
});






const app = express();

app.use(express.static(path.join(__dirname, 'dist'))); // gives access the express to dist folder
app.use(bodyParser.urlencoded({extended:true}));  // parses the text that is url encoded data
app.use(bodyParser.json());                     // parses the text as json


app.get('*', function(req, res){
	res.sendFile(path.join(__dirname, 'dist/index.html'));
});

app.get('/', function(req, res){
	res.send('It is / path, It is working');
});

var updateValue = 'This is new text';
fs.appendFile('anc.txt', updateValue , function(err){  // updating value
	if(err) throw err ;
	console.log('Updated');
});

//fs.unlink('anc.txt', function(err){
//	if(err) throw err;                         // delating values
//	console.log('File Deleted');
//});

app.post('/first-page', function(req, res){
	console.log(req.body);
});



app.set('port',process.env.PORT||3000);
app.listen(app.get('port'), function(req, res){
	console.log('listening');
});