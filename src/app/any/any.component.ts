import { Component, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {trigger,state,style,animate,transition,} from '@angular/animations';


@Component({
  selector: 'app-any',
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        height: '800px',
        opacity: 0.1,
        backgroundColor: 'red'
      })),
      state('closed', style({
        height: '80px',
        opacity: 0.5,
        backgroundColor: 'black'
      })),
      transition('open => closed', [
        animate('1s')
      ]),
      transition('closed => open', [
        animate('5s')
      ]),
    ]),
  ],
  templateUrl: './any.component.html',
  styleUrls: ['./any.component.scss']
})
export class AnyComponent implements OnInit {
 isOpen: boolean= true;

  constructor() { }


  toggle() {
    this.isOpen = !this.isOpen;
  }

  ngOnInit() {
  }

}
