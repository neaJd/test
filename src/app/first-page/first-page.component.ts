import { Component, OnInit } from '@angular/core';
import {trigger,state,style,animate,transition,} from '@angular/animations';

@Component({
  selector: 'app-first-page',
  templateUrl: './first-page.component.html',
  styleUrls: ['./first-page.component.scss'],
animations: [
    trigger('start', [
      // ...
      state('open', style({
        height: '800px',
        opacity: 0.1,
        color: 'white',
        
	 transform: 'translateX(60px)',

      })),
      state('closed', style({
        height: '80px',
        opacity: 1,
        color: 'white',
        transform: 'translateX(0)',
          })),

     
       
      transition('open => closed', [
        animate('2s')
      ]),
      transition('closed => open', [
        animate('1s')
      ]),
    ]),
  ],

})
export class FirstPageComponent implements OnInit {
isStart:boolean= false;
  constructor() { }

toggle(){
	this.isStart=!this.isStart;
}
  ngOnInit() {
  }

}
