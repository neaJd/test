import { Component, OnInit } from '@angular/core';
import {trigger,state,style,animate,transition,} from '@angular/animations';


@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
  animations: [
    trigger('start', [
      // ...
      state('open', style({
       // height: '800px',
        opacity: 0,
        color: 'white',
        
  // transform: 'translateX(60px)',

      })),
      state('closed', style({
      //  height: '80px',
        opacity: 1,
        color: 'red',
       // transform: 'translateX(0)',
          })),

     
       
      transition('open => closed', [
        animate('2s')
      ]),
      transition('closed => open', [
        animate('5s')
      ]),
    ]),
    trigger('second', [
      // ...
      state('open', style({
       // height: '800px',
        opacity: 0,
        color: 'red',
        

      })),
      state('closed', style({
      //  height: '80px',
        opacity: 1,
        color: 'white',
     
          })),

     
       
      transition('open => closed', [
        animate('5s')
      ]),
      transition('closed => open', [
        animate('10s')
      ]),
    ]),
  ],

})
export class ContainerComponent implements OnInit {
  videoSource:"https://www.youtube.com/watch?v=5w";
  touch:boolean = true;
  
   
  constructor() { }

toggle(){
	this.touch = false;
}



  ngOnInit() {

  
  }

}
