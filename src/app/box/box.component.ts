import { Component, OnInit } from '@angular/core';
import { trigger, state, transition, style, animate} from '@angular/animations';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.scss'],
  animations:[ 
             trigger('fade',[
            
            state('void', style({backgroundColor:'black', opacity:0})),

             transition(':enter, :leave', [ animate(3000) ]),

            

             
])
]
          })
export class BoxComponent implements OnInit {
hello:boolean=true;
  constructor() { }
toggle(){
	this.hello=!this.hello;
}
  ngOnInit() {
  }

}
