import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstPageComponent } from './first-page/first-page.component';
import { EasyComponent } from './easy/easy.component';
import { ContainerComponent } from './container/container.component';
//import { TodoComponent } from './todo/todo.component';
import { AnyComponent } from './any/any.component';
import { BoxComponent } from './box/box.component';

const routes: Routes = [

{path:'', redirectTo:'/first-page', pathMatch:'full'},
{path:'first-page', component:FirstPageComponent},
{path:'input', component: BoxComponent},
{path:'container',component: ContainerComponent},
{path:'easy', component: EasyComponent},
{path:"**", component:AnyComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
export const routingComponents = [FirstPageComponent, EasyComponent,ContainerComponent,AnyComponent]

